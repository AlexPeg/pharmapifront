import React, { useState } from 'react';
import Menu from './Menu';
import PharmaList from './PharmaList';
import './App.css';

function App() {

  const [page, setPage] = useState('page1');

  const doClick = id => setPage(id);

  return (
    <div className="App">
      <header className="App-header">
        <nav>
          <Menu title="Liste des pharmacies" id="page1" onClick={doClick} />
          <Menu title="menu 2" id="page2" onClick={doClick} />
        </nav>
      </header>
      <main>

        {page === "page1" &&

          <PharmaList />

        }

        {page === "page2" &&
        
          <p>Ici ma page 2</p>

        }


      </main>
    </div>
  )
}



export default App;
