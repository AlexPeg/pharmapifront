import React, { useState, useEffect } from 'react';
import axios from 'axios';

/*
function PharmaList() {

    const [pharmacies, setPharmacies] = useState([]);

    useEffect(() => {
        axios.get('http://127.0.0.1:8080/pharma')
            .then(pharmapi => {
                console.log(pharmapi.data);
                setPharmacies(pharmapi.data);
            });
    }, []);

    return (
        <article>
            <h2>Liste des pharmacies</h2>
            <ul>
                {pharmacies.map(pharmacie => (
                    <li key={pharmacie.id}>
                        {pharmacie.nom}
                    </li>
                ))}
            </ul>
        </article>
    )
}
*/

class PharmaList extends React.Component {

    constructor(props){
        super(props);
        this.state = {pharmacies: []};
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:8080/pharma')
            .then(pharmapi => {
                console.log(pharmapi.data);
                this.setState({pharmacies : pharmapi.data});
            });
    }

    render(){
        return (
            <article>
                <h2>Liste des pharmacies</h2>
                <ul>
                    {this.state.pharmacies.map(pharmacie => (
                        <li key={pharmacie.id}>
                            {pharmacie.nom}
                        </li>
                    ))}
                </ul>
            </article>
        )
    }
}

export default PharmaList;