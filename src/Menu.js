
/*

function Menu(props) {
  return (
    <button onClick={() => props.onClick(props.id)}>
    {props.title}
    </button>
    )
  }
  
*/

import React from "react";

class Menu extends React.Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <button onClick={() => this.props.onClick(this.props.id)}>
        {this.props.title}
      </button>
    )
  }
}

export default Menu;